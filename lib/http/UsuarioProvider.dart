import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:request_http/storage/SharedPreferences.dart';

class UsuarioProvider{

  final prefs = new PreferenciasUsuario();
  //nueva variable booleana
  bool trustSelfSigned = true;

  final String URL_PIN = "https://enroll.dev.unipagos.com/signCertificate";  //QA
   String URL = 'https://enroll.dev.unipagos.com/api/v2.0/devices';  //QA

  Future<Map<String,dynamic>>registroWS(String phoneNumber)async{
    final bodyRequest = {
      'mdn':phoneNumber,
      'platform':"android",
      "packageId":"com.unipagos",
      "walletVersion":"2.0.1"// campos faltantes

    };

 HttpClient client = new HttpClient()
 ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

  final request = await client.postUrl(Uri.parse('https://enroll.unipagos.com/api/v2.0/devices'));

    request.headers.set('content-type', 'application/json');
    request.headers.set('Accept', 'application/json');

    request.add(utf8.encode(json.encode(bodyRequest)));

    HttpClientResponse response = await request.close();

    String reply = await response.transform(utf8.decoder).join();

    print('reply : $reply');

    Map<String,dynamic> decodeResponse = json.decode(reply);

    print('decodeResponse : $decodeResponse');

    if(decodeResponse['_resultType'] == "SUCCESS"){
      //salvar el número
      prefs.telefono = phoneNumber;
      print("OTP: ${decodeResponse['otp']}");
      return {"status":true};
    }else{ //troubles
      return{"status":false,"error":decodeResponse["_resultDesc"]};
    }
  }

  Future<Map<String,dynamic>>enviarPINWS(String deviceId,String csr,String codigo)async{


    final bodyRequest = {
      'csr':csr,
      "otp":codigo,
      "mdn":deviceId
    };

    print('csr : $csr');
    print('codigo : $codigo');
    print('deviceId : $deviceId');

    HttpClient client = new HttpClient()
    ..badCertificateCallback = ((X509Certificate cert, String host, int port) => trustSelfSigned);

    final request = await client.postUrl(Uri.parse('https://enroll.unipagos.com/signCertificate'));

    request.headers.set('content-type', 'application/json');
    request.headers.set('Accept', 'application/json');

    request.add(utf8.encode(json.encode(bodyRequest)));

    HttpClientResponse response = await request.close();

    final reply = await response.transform(utf8.decoder).join();

    Map<String,dynamic> decodeResponse = json.decode(reply);

    print('decodeResponse : $decodeResponse');

    if(decodeResponse['_resultType'] == "SUCCESS"){
      //salvar el crt
      prefs.crt = decodeResponse['crt'];
      return {"status":true};
    }else{ //troubles
      return{"status":false,"error":decodeResponse["_resultDesc"]};
    }
  }

  
}