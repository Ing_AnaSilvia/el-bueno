import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET del _colorSecundario
  get colorSecundario {
    return _prefs.getBool('colorSecundario') ?? false;
  }

  set colorSecundario( bool value ) {
    _prefs.setBool('colorSecundario', value);
  }


  // GET y SET del nombreUsuario
  get telefono {
    return _prefs.getString('phone') ?? '';
  }

  set telefono( String value ) {
    _prefs.setString('phone', value);
  }

  // GET y SET del crt
  get crt {
    return _prefs.getString('crt') ?? '';
  }

  set crt( String value ) {
    _prefs.setString('crt', value);
  }

}