import "package:pointycastle/export.dart";
import 'package:x509csr/x509csr.dart';
import 'package:asn1lib/asn1lib.dart';

String generateCrs(String number){

AsymmetricKeyPair keyPair = rsaGenerateKeyPair();

ASN1ObjectIdentifier.registerFrequentNames();
  Map<String, String> dn = {
    "CN": "www.unipagos.com",
    "O": "Mexico",
    "L": "CDMx",
    "ST": "CDMX",
    "C": "CDMX",
    "DN": "$number"
  };

  ASN1Object encodedCSR = makeRSACSR(dn, keyPair.privateKey, keyPair.publicKey);


  final salidaCSR = encodeCSRToPem(encodedCSR);

  print('salida  : $salidaCSR');

  print(encodeCSRToPem(encodedCSR));
  //print(encodeRSAPublicKeyToPem(keyPair.publicKey));
  //print(encodeRSAPrivateKeyToPem(keyPair.privateKey));

  print('encodedCSR : $encodedCSR');

  return  salidaCSR;
}

String base64CRS(String crs){
  String csrBase64;

  print('csrBase64: $csrBase64');
  csrBase64 = ("-----BEGIN CERTIFICATE REQUEST-----"  '\n' + crs + '\n' + "-----END CERTIFICATE REQUEST-----");
  print('csrBase64 : $csrBase64');
  return csrBase64;
}