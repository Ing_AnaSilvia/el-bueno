import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:request_http/encrypt/GenCsr.dart';
import 'package:request_http/http/UsuarioProvider.dart';
import 'package:request_http/storage/SharedPreferences.dart';
import 'package:request_http/utils/utils.dart';

class PinScreen extends StatelessWidget {

  final userProv = new UsuarioProvider();

  TextEditingController codigo = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
      title: Text("Validación de Número"),
      ),
        body:
        Center(
        child:
        Column(
          children: <Widget>[
            SizedBox( height: 40.0 ),
            _crearEditText( context ),
            SizedBox( height: 40.0 ),
            _aceptButton(context),
           
          ],
        )
    )
    );
  }

  Widget _crearEditText(BuildContext ctx) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextField(
            keyboardType: TextInputType.phone,
            decoration: InputDecoration(
                icon: Icon( Icons.phone_android, color: Colors.black),
                hintText: 'Ej: 5551231231',
                labelText: 'Código Recibido',
                errorText: ""
            ),
            controller: codigo,
          ),
        );
  }

  Widget _aceptButton(BuildContext ctx){
    return RaisedButton(
        child: Container(
          child: Center(
              child:
              Text('Aceptar')),
          width: 100.0,
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
        ),
        elevation: 3.0,
        color: Colors.deepOrange,
        textColor: Colors.white,
        onPressed: ()=> _sendPINWS(ctx,codigo.text)
    );
  }

  _sendPINWS(BuildContext ctx,String pin)async{

    String number = PreferenciasUsuario().telefono;
    final crsBase64 =   generateCrs(number);


    Map response = await userProv.enviarPINWS(number,crsBase64,pin);

    print("response  :  $response" );
    if (response["status"]){
      Navigator.pushNamed(ctx, 'pin');
    }else{ //mostrar Alerta
      showAlert(ctx,response["error"]);
    }
  }

}