import 'package:flutter/material.dart';

class ChangePinScreen extends StatelessWidget {

  TextEditingController codigo_pin = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Ingrese un pin"),
        ),
        body:
        Center(
            child:
            Column(
              children: <Widget>[
                SizedBox( height: 40.0 ),
                _crearEditText( context ),
                SizedBox( height: 40.0 ),
                _aceptButton(context),

              ],
            )
        )
    );
  }

  Widget _crearEditText(BuildContext ctx) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      child: TextField(
        keyboardType: TextInputType.phone,
        decoration: InputDecoration(
            icon: Icon( Icons.phone_android, color: Colors.black),
            hintText: '5 números',
            labelText: 'Ingrese un pin',
            errorText: ""
        ),
        controller: codigo_pin,
      ),
    );
  }

  Widget _aceptButton(BuildContext ctx){
    return RaisedButton(
        child: Container(
          child: Center(
              child:
              Text('Aceptar')),
          width: 100.0,
        ),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0)
        ),
        elevation: 3.0,
        color: Colors.deepOrange,
        textColor: Colors.white,
        onPressed: ()=> _sendPINWS(ctx,codigo_pin.text)
    );
  }

  _sendPINWS(BuildContext ctx,String pin)async{

  }

}